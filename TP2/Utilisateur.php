<?php

use model\model;

class Utilisateur {
    private string $login;
    private string $nom;
    private string $prenom;

    /**
     * @param string $login
     * @param string $nom
     * @param string $prenom
     */
    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public static function builder(array $utilisateurFormatTableau) : Utilisateur {
        return new static($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prénom']);
    }

    public static function getUtilisateurs(){
        $tab = [];
        $pdo = Model::getPdo();
        $pdoStatement=$pdo->query("SELECT * FROM td2_utilisateur");
        foreach($pdoStatement as $utilisateurFormatTableau){
            $tab[]=self::builder($utilisateurFormatTableau);
        }
        return $tab;
    }

    // AFFICHAGE
    public function afficher(){
        echo "<p> {$this -> login}, {$this -> nom}, {$this -> prenom}";
    }
}

?>