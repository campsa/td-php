<?php

use model\model;
use model\ModelVoiture;

class Voiture {

    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque() : string{
        return $this->marque;
    }

    // un setter
    public function setMarque(string $marque) : void {
        $this->marque = $marque;
    }

    // un constructeur
    public function __construct($marque, $couleur, $immatriculation, $nbSieges) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = substr($immatriculation,0,9);
        $this->nbSieges = $nbSieges;
    }

    public static function construire(array $voitureFormatTableau) : ModelVoiture {
        $voiture = new static($voitureFormatTableau['marqueBDD'], $voitureFormatTableau['couleurBDD'], $voitureFormatTableau['immatriculationBDD'], $voitureFormatTableau['nbSiegeBDD'],);
        return $voiture;
    }

    public static function getVoitures()
    {
        $tabv = [];
        $pdo = Model::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM voiture");
        foreach ($pdoStatement as $voitureFormatTableau) {
            $tabv[] = self::construire($voitureFormatTableau);
        }
        return $tabv;
    }


    // une methode d'affichage.
    public function afficher() {
        $affiche = "<p> marque : $this->marque </p> <p>couleur : $this->couleur </p> <p> immatricu : $this->immatriculation </p> <p>nbSieges : $this->nbSieges </p>";
        return $affiche;

    }

    public function getCouleur() : string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur) : void
    {
        $this->couleur = $couleur;
    }

    public function getImmatriculation() : string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(string $immatriculation) : void
    {
        $this->immatriculation = substr($immatriculation,0,9);
    }


    public function getNbSieges(): int
    {
        return $this->nbSieges;
    }


    public function setNbSieges(int $nbSieges): void
    {
        $this->nbSieges = $nbSieges;
    }





}
