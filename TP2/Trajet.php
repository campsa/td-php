<?php

use model\model;

class Trajet {
    private int $id;
    private string $depart;
    private string $arrivee;
    private string $date;
    private string $nbplaces;
    private float $prix;
    private string $conducteur_login;

    /**
     * @param int $id
     * @param string $depart
     * @param string $arrivee
     * @param string $date
     * @param string $nbplaces
     * @param float $prix
     * @param string $conducteur_login
     */
    public function __construct(int $id, string $depart, string $arrivee, string $date, string $nbplaces, float $prix, string $conducteur_login)
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->nbplaces = $nbplaces;
        $this->prix = $prix;
        $this->conducteur_login = $conducteur_login;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDepart(): string
    {
        return $this->depart;
    }

    /**
     * @param string $depart
     */
    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    /**
     * @return string
     */
    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    /**
     * @param string $arrivee
     */
    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getNbplaces(): string
    {
        return $this->nbplaces;
    }

    /**
     * @param string $nbplaces
     */
    public function setNbplaces(string $nbplaces): void
    {
        $this->nbplaces = $nbplaces;
    }

    /**
     * @return float
     */
    public function getPrix(): float
    {
        return $this->prix;
    }

    /**
     * @param float $prix
     */
    public function setPrix(float $prix): void
    {
        $this->prix = $prix;
    }

    /**
     * @return string
     */
    public function getConducteurLogin(): string
    {
        return $this->conducteur_login;
    }

    /**
     * @param string $conducteur_login
     */
    public function setConducteurLogin(string $conducteur_login): void
    {
        $this->conducteur_login = $conducteur_login;
    }

    public static function builder(array $trajetFormatTableau) : Trajet {
        return new static($trajetFormatTableau['id'], $trajetFormatTableau['depart'], $trajetFormatTableau['arrivee'], $trajetFormatTableau['date'], $trajetFormatTableau['nbPlaces'], $trajetFormatTableau['prix'], $trajetFormatTableau['conducteur_login']);
    }

    public static function getTrajets(){
        $tab = [];
        $pdo = Model::getPdo();
        $pdoStatement=$pdo -> query("SELECT * FROM td2_trajet");
        foreach($pdoStatement as $trajetFormatTableau){
            $tab[]=self::builder($trajetFormatTableau);
        }
        return $tab;
    }

    // AFFICHAGE
    public function afficher() {
        echo "<p> {$this -> id}, {$this -> depart}, {$this -> arrivee}, {$this -> date}, {$this -> prix}, {$this -> nbplaces}, {$this -> conducteur_login}</p>";
    }
}

?>