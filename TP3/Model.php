<?php

use config\Conf;

require_once "Conf.php";

    class model{

        //établie la connextion entre la base de donnée et nous
        private PDO $pdo;

        private static ?model\Model $instance = null;



        public function __construct()
        {
            $hostname = Conf::getHostName();
            $database_name = Conf::getDataBase();
            $login = Conf::getLogin();
            $password = Conf::getPassword();
            $this->pdo = new PDO("mysql:host=$hostname;dbname=$database_name", $login, $password,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }

        public static function getPdo() : PDO {
            return static::getInstance()->pdo;
        }

        private static function getInstance() : model\Model {
            // L'attribut statique $pdo s'obtient avec la syntaxe static::$pdo
            // au lieu de $this->pdo pour un attribut non statique

            if (is_null(static::$instance))
                // Appel du constructeur
                static::$instance = new model\Model();
            return static::$instance;
        }



    }