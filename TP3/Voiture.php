<?php

use model\model;
use model\ModelVoiture;

require_once 'Model.php';

class Voiture {


    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque() : string{
        return $this->marque;
    }

    // un setter
    public function setMarque(string $marque) : void {
        $this->marque = $marque;
    }

    // un constructeur
    public function __construct($marque, $couleur, $immatriculation, $nbSieges) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = substr($immatriculation,0,9);
        $this->nbSieges = $nbSieges;
    }

    public static function construire(array $voitureFormatTableau) : ModelVoiture {
        $voiture = new static($voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges'],);
        return $voiture;
    }

    public static function getVoitures()
    {
        $tabv = [];
        $pdo = Model::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM voiture");
        foreach ($pdoStatement as $voitureFormatTableau) {
            $tabv[] = self::construire($voitureFormatTableau);
        }
        return $tabv;
    }

    public static function getVoitureParImmat(string $immatriculation) : ModelVoiture {
        $sql = "SELECT * from voiture WHERE immatriculation=:immatriculationTag";
        // Préparation de la requête
        $pdoStatement = Model::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();

        return static::construire($voiture);
    }

    public function sauvegarder() : bool{
        $requete = "INSERT INTO voiture (marque,couleur,immatriculation,nbSieges) VALUES (:marqueTag,:couleurTag,:immatriculationTag,:nbSiegesTag)";

        $pdoStatement = Model::getPdo()->prepare($requete);
        $values = array(
            "marqueTag" => $this->marque,
            "couleurTag" => $this->couleur,
            "immatriculationTag" => $this->immatriculation,
            "nbSiegesTag" => $this->nbSieges
        );
        try{
            $pdoStatement->execute($values);
        }
        catch (Exception $e){
            return false;
        }
        return true;

    }



    // une methode d'affichage.
    public function afficher() {
        $affiche = "<p> marque : $this->marque </p> <p>couleur : $this->couleur </p> <p> immatricu : $this->immatriculation </p> <p>nbSieges : $this->nbSieges </p>";
        return $affiche;

    }

    public function getCouleur() : string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur) : void
    {
        $this->couleur = $couleur;
    }

    public function getImmatriculation() : string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(string $immatriculation) : void
    {
        $this->immatriculation = substr($immatriculation,0,9);
    }


    public function getNbSieges(): int
    {
        return $this->nbSieges;
    }


    public function setNbSieges(int $nbSieges): void
    {
        $this->nbSieges = $nbSieges;
    }





}
