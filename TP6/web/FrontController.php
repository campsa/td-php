
<?php
    require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
    use App\Covoiturage\Controller\ControllerVoiture;

    // instantiate the loader
    $loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
    // register the base directories for the namespace prefix
    $loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');
    // register the autoloader
    $loader->register();



    // On recupère l'action passée dans l'URL

    //controller
    if(!isset($_GET['controller'])){
        $controller = "voiture";
        $controllerClassName = "App\Covoiturage\Controller\Controller" . ucfirst($controller) ;
    }
    else{
        $controller = $_GET['controller'];
        $controllerClassName = "App\Covoiturage\Controller\Controller" . ucfirst($controller);
    }

    //action
    if(!isset($_GET['action'])){
        $action ="readAll";
 
    }else{
        $action = $_GET['action'];
    }

    if(class_exists($controllerClassName) and in_array($action,get_class_methods($controllerClassName))){
        $controllerClassName::$action();

    } else {
        ControllerVoiture::error("Cette page n'existe pas");
    }




?>

