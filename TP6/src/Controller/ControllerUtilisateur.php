<?php

namespace App\Covoiturage\Controller;
use App\Covoiturage\Model\Repository\UtilisateurRepository;
use App\Covoiturage\Model\DataObject\Utilisateur;
use App\Covoiturage\Model\Repository\VoitureRepository;


class ControllerUtilisateur {

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../src/view/$cheminVue"; // Charge la vue
    }


    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        //$utilisateurs = UtilisateurRepository::getUtilisateurs(); //appel au modèle pour gerer la BD
        $utilisateurs = (new UtilisateurRepository())->selectAll();
        self::afficheVue('/view.php',['utilisateurs' => $utilisateurs,"pagetitle" => "Liste des utilisateurs", "cheminVueBody" => "utilisateur/list.php"]);  //"redirige" vers la vue
    }

    public static function read() : void{

        if(isset($_GET['login'])) {
            $login=$_GET['login'];
            //$utilisateurs = VoitureRepository::getVoitureParImmat($immatriculation);
            $utilisateurs = (new VoitureRepository()) ->select($login);
            if (!$utilisateurs) {
                //self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "voiture/error.php"]);
                self::error("aucun login renseigné");
            } else {
                self::afficheVue('/view.php', ['utilisateurs' => $utilisateurs, "pagetitle" => "Utilisateur", "cheminVueBody" => "utilisateur/detail.php"]);
            }
        }
        else{
            self::error("veuillez renseigné un login valide");
        }
    }
    /*
    public static function read() : void{

        if(isset($_GET['immatriculation'])) {
            $immatriculation=$_GET['immatriculation'];
            $utilisateurs = UtilisateurRepository::getUtilisateurParImmat($immatriculation);
            if (!$utilisateurs) {
                //self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "utilisateur/error.php"]);
                self::error("aucune utilisateur renseigné");
            } else {
                self::afficheVue('/view.php', ['utilisateurs' => $utilisateurs, "pagetitle" => "Utilisateur", "cheminVueBody" => "utilisateur/detail.php"]);
            }
        }
        else{
            self::error("veuillez renseigné une immatriculation valide");
        }
    }

    public static function create() : void{
        self::afficheVue('/view.php',["pagetitle"=>"Creer une utilisateur","cheminVueBody"=>"utilisateur/create.php"]);
    }

    public static function created() : void{

        if (isset($_GET['marque']) && isset($_GET['couleur']) && isset($_GET['immatriculation']) && isset($_GET['nbSieges'])) {
            $marque = $_GET['marque'];
            $couleur = $_GET['couleur'];
            $imm = $_GET['immatriculation'];
            $sieges = $_GET['nbSieges'];

            $v = new Utilisateur($marque, $couleur, $imm, $sieges);
            $bool = UtilisateurRepository::sauvegarder($v);
            if($bool) {
                self::afficheVue('/view.php',["pagetitle" => "Création utilisateur","cheminVueBody" =>"utilisateur/created.php"]);

            }
            else{
                self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "utilisateur/error.php"]);
            }
        } else {
            echo "Wrong data !!";
        }

    }

    public static function delete() {
        if (isset($_GET['immatriculation'])) {
            if(UtilisateurRepository::delete($_GET['immatriculation'])>=1) {
                self::afficheVue('/view.php', ["pagetitle" => "supprimé", "cheminVueBody" => "utilisateur/deleted.php"]);
            }
            else {
                self::error("Veuillez renseigner l'immatriculation !!");
            }
        } else {
            self::error("Veuillez renseigner l'immatriculation !!");
        }
    }

    public static function update() : void{


        if(isset($_GET['immatriculation'])) {
            $immatriculation=$_GET['immatriculation'];
            $utilisateur = UtilisateurRepository::getUtilisateurParImmat($immatriculation);
            if(isset($immatriculation) && isset($utilisateur)) {
                self::afficheVue('/view.php', ["pagetitle" => "Update d'un utilisateur", "cheminVueBody" => "utilisateur/update.php", "v" => $utilisateur, ]);
            }
            else{
                self::error("utilisateur inexistante");
            }
        }
        else{
            self::error("veuillez renseigné une immatriculation ");
        }
    }

    public static function updated() : void{

        if (isset($_GET['marque']) && isset($_GET['couleur']) && isset($_GET['immatriculation']) && isset($_GET['nbSieges'])) {
            $marque = $_GET['marque'];
            $couleur = $_GET['couleur'];
            $imm = $_GET['immatriculation'];
            $sieges = $_GET['nbSieges'];


            $bool = UtilisateurRepository::misaJour(Utilisateur::getUtilisateurParImmat($_GET['immatriculation']));
            if($bool) {
                self::afficheVue('/view.php',["pagetitle" => "Création utilisateur","cheminVueBody" =>"utilisateur/updated.php","utilisateur"=>Utilisateur::getUtilisateurParImmat($_GET['immatriculation'])]);


            }
            else{
                self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "utilisateur/error.php"]);
            }
        } else {
            self::error("donne éroné");
        }

    }
    */
    public static function error(string $errorMessage=""){
        self::afficheVue('/view.php',["pagetitle"=>"erreur","cheminVueBody"=>"utilisateur/error.php", 'errorMessage' => $errorMessage]);
    }





}
?>