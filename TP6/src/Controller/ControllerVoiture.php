<?php

namespace App\Covoiturage\Controller;
use App\Covoiturage\Model\Repository\AbstractRepository;
use App\Covoiturage\Model\Repository\VoitureRepository;
use App\Covoiturage\Model\DataObject\Voiture;


class ControllerVoiture {

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../src/view/$cheminVue"; // Charge la vue
    }


    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        //$voitures = AbstractRepository::getVoitures(); //appel au modèle pour gerer la BD
        $voitures = (new VoitureRepository())->selectAll();

        self::afficheVue('/view.php',['voitures' => $voitures,"pagetitle" => "Liste des voitures", "cheminVueBody" => "voiture/list.php"]);  //"redirige" vers la vue
    }

    public static function read() : void{

        if(isset($_GET['immatriculation'])) {
            $immatriculation=$_GET['immatriculation'];
            //$voitures = VoitureRepository::getVoitureParImmat($immatriculation);
            $voitures = (new VoitureRepository()) ->select($immatriculation);
            if (!$voitures) {
                //self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "voiture/error.php"]);
                self::error("aucune voiture renseigné");
            } else {
                self::afficheVue('/view.php', ['voitures' => $voitures, "pagetitle" => "Voiture", "cheminVueBody" => "voiture/detail.php"]);
            }
        }
        else{
            self::error("veuillez renseigné une immatriculation valide");
        }
    }

    public static function create() : void{
        self::afficheVue('/view.php',["pagetitle"=>"Creer une voiture","cheminVueBody"=>"voiture/create.php"]);
    }

    public static function created() : void{

        if (isset($_GET['marque']) && isset($_GET['couleur']) && isset($_GET['immatriculation']) && isset($_GET['nbSieges'])) {
            $marque = $_GET['marque'];
            $couleur = $_GET['couleur'];
            $imm = $_GET['immatriculation'];
            $sieges = $_GET['nbSieges'];

            $v = new Voiture($marque, $couleur, $imm, $sieges);
            $bool = VoitureRepository::sauvegarder($v);
            if($bool) {
                self::afficheVue('/view.php',["pagetitle" => "Création voiture","cheminVueBody" =>"voiture/created.php"]);

            }
            else{
                self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "voiture/error.php"]);
            }
        } else {
            echo "Wrong data !!";
        }

    }

    public static function delete() {
        if (isset($_GET['immatriculation'])) {
            if(VoitureRepository::delete($_GET['immatriculation'])>=1) {
                self::afficheVue('/view.php', ["pagetitle" => "supprimé", "cheminVueBody" => "voiture/deleted.php"]);
            }
            else {
                self::error("Veuillez renseigner l'immatriculation !!");
            }
        } else {
            self::error("Veuillez renseigner l'immatriculation !!");
        }
    }

    public static function update() : void{


        if(isset($_GET['immatriculation'])) {
            $immatriculation=$_GET['immatriculation'];
            $voiture = (new VoitureRepository())->select($immatriculation);
            if(isset($immatriculation) && isset($voiture)) {
                self::afficheVue('/view.php', ["pagetitle" => "Update d'un voiture", "cheminVueBody" => "voiture/update.php", "v" => $voiture, ]);
            }
            else{
                self::error("voiture inexistante");
            }
        }
        else{
            self::error("veuillez renseigné une immatriculation ");
        }
    }

    public static function updated() : void{

        if (isset($_GET['marque']) && isset($_GET['couleur']) && isset($_GET['immatriculation']) && isset($_GET['nbSieges'])) {
            $marque = $_GET['marque'];
            $couleur = $_GET['couleur'];
            $imm = $_GET['immatriculation'];
            $sieges = $_GET['nbSieges'];


            //$bool = VoitureRepository::misaJour(Voiture::getVoitureParImmat($_GET['immatriculation']));
            $bool = VoitureRepository::misaJour((new VoitureRepository())->select($_GET['immatriculation']));
            if($bool) {
                self::afficheVue('/view.php',["pagetitle" => "Création voiture","cheminVueBody" =>"voiture/updated.php","voiture"=>Voiture::getVoitureParImmat($_GET['immatriculation'])]);


            }
            else{
                self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "voiture/error.php"]);
            }
        } else {
                self::error("donne éroné");
        }

    }

    public static function error(string $errorMessage=""){
        self::afficheVue('/view.php',["pagetitle"=>"erreur","cheminVueBody"=>"voiture/error.php", 'errorMessage' => $errorMessage]);
    }





}
?>

