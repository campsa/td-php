<?php

namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\DataObject\AbstractDataObject;
use App\Covoiturage\Model\DataObject\Voiture;


class VoitureRepository extends AbstractRepository {

    public function getNomTable() : string{
        return "voiture";
    }

    public function getNomClePrimaire(): string{
        return "immatriculation";
    }
    /*
    public static function getVoitureParImmat(string $immatriculation): ?Voiture
    {
        $sql = "SELECT * from voiture WHERE immatriculation=:immatriculationTag";
        // Préparation de la requête
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();
        if (!$voiture) {
            return null;
        }

        return static::construire($voiture);
    }
    */

    public static function sauvegarder($voiture): bool
    {
        $requete = "INSERT INTO voiture (marque,couleur,immatriculation,nbSieges) VALUES (:marqueTag,:couleurTag,:immatriculationTag,:nbSiegesTag)";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($requete);
        $values = array(
            "marqueTag" => $voiture->getMarque(),
            "couleurTag" => $voiture->getCouleur(),
            "immatriculationTag" => $voiture->getImmatriculation(),
            "nbSiegesTag" => $voiture->getnbSieges()
        );
        try {
            $pdoStatement->execute($values);
        } catch (Exception $e) {
            return false;
        }
        return true;

    }
    public static function misaJour(Voiture $voiture): bool
    {
        //$requete = "INSERT INTO voiture (marque,couleur,immatriculation,nbSieges) VALUES (:marqueTag,:couleurTag,:immatriculationTag,:nbSiegesTag)";
        $requete = "UPDATE voiture
                    SET marque = :marqueTag, couleur = :couleurTag,immatriculation = :immatriculationTag , nbSieges = :nbSiegesTag
                    WHERE immatriculation = :immatriculationTag";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($requete);
        $values = array(
            "marqueTag" => $_GET['marque'],
            "couleurTag" => $_GET['couleur'],
            "immatriculationTag" => $voiture->getImmatriculation(),
            "nbSiegesTag" => $_GET['nbSieges']
        );
        try {
            $pdoStatement->execute($values);
        } catch (Exception $e) {
            return false;
        }
        return true;

    }

    public function construire(array $voitureFormatTableau): Voiture
    {
        $voiture = new Voiture($voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges'],);
        return $voiture;
    }

    public static function delete($immatriculation): int
    {
        $sql = "DELETE FROM voiture WHERE immatriculation = '$immatriculation'";
        $pdoStatement = DatabaseConnection::getPdo()->query($sql);
        return $pdoStatement->rowCount();
    }

}