<?php

namespace App\Covoiturage\Model\Repository;
use App\Covoiturage\Model\DataObject\Utilisateur;


class UtilisateurRepository extends AbstractRepository {

    public function getNomClePrimaire(): string
    {
        return "Login";
    }


    public function getNomTable(): string
    {
       return "utilisateur";
    }

    public function construire(array $utilisateurTableau) : Utilisateur {
        return new Utilisateur(
            $utilisateurTableau["login"],
            $utilisateurTableau["nom"],
            $utilisateurTableau["prenom"]
        );
    }


}