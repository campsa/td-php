<?php
    namespace App\Covoiturage\Model\Repository;
    use App\Covoiturage\Model\DataObject\AbstractDataObject;

    abstract class AbstractRepository{

        protected abstract function getNomTable(): string;

        protected abstract function construire(array $objetFormatTableau) : AbstractDataObject;

        protected abstract function getNomClePrimaire(): string;


        public function selectAll() : array
        {
            $tabvObject = [];
            $pdo = DatabaseConnection::getPdo();
            $pdoStatement = $pdo->query("SELECT * FROM " . $this->getNomTable());
            foreach ($pdoStatement as $objectFormatTableau) {
                $tabvObject[] = $this->construire($objectFormatTableau);
            }
            return $tabvObject;
        }

        public function select(string $valeurClePrimaire): ?AbstractDataObject{
            $sql = "SELECT * from " . $this->getNomTable() . " WHERE " .$this->getNomClePrimaire() ."=:clePrimaireTag";
            // Préparation de la requête
            $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

            $values = array(
                "clePrimaireTag" => $valeurClePrimaire,
                //nomdutag => valeur, ...
            );
            // On donne les valeurs et on exécute la requête
            $pdoStatement->execute($values);

            // On récupère les résultats comme précédemment
            // Note: fetch() renvoie false si pas de voiture correspondante
            $object = $pdoStatement->fetch();
            if (!$object) {
                return null;
            }

            return $this->construire($object);
        }


    }

