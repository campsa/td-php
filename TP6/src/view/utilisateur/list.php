
<?php
    foreach ($utilisateurs as $utilisateur) {
        $utilisateurIdHtml = htmlspecialchars($utilisateur->getLogin());
        $utilisateurIdUrl = rawurlencode($utilisateur->getLogin());

        echo
            '<p> '
            . "<a  href=\"FrontController.php?action=update&login={$utilisateurIdUrl}\"> ✎</a>"
            . 'Utilisateur de login : '
            . "<a  href=\"FrontController.php?action=read&login={$utilisateurIdUrl}\"> {$utilisateurIdHtml} </a>"
            . "<a  href=\"FrontController.php?action=delete&login={$utilisateurIdUrl}\"> 🗑️</a>"
            . '</p>' . "\n";

    }
?>
