
<form method="get" action="FrontController.php">
    <fieldset>
        <legend>Modification de la voiture immatricule :</legend>
        <p>
            <label for="marque_id">marque</label> :
            <input type="text" value="<?php echo $v->getMarque(); ?>" name="marque" id="marque_id" required/>
        </p>
        <p>
            <label for="couleur_id">couleur</label> :
            <input type="text" value="<?php echo $v->getCouleur(); ?>" name="couleur" id="couleur_id" required/>
        </p>
        <p>
            <label for="immat_id">Immatriculation</label> :
            <input type="text" value="<?php echo $v->getImmatriculation(); ?>"  name="immatriculation" id="immat_id" readonly/>
        </p>
        <p>
            <label for="nbSieges_id">nbSieges</label> :
            <input type="text" value="<?php echo $v->getNbSieges(); ?>" name="nbSieges" id="nbSieges_id" required/>
        </p>

        <p>
            <input type='hidden' name='action' value='updated'>

            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>