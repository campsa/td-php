
<?php
    foreach ($voitures as $voiture) {
        //echo '<p> Voiture d\'immatriculation ' . $voiture->getImmatriculation() . '.</p>';
        $immatriculationHtml = htmlspecialchars($voiture->getImmatriculation());
        $immatriculationUrl = rawurlencode($voiture->getImmatriculation());

        echo
            '<p> '
            . "<a  href=\"FrontController.php?action=update&immatriculation={$immatriculationUrl}\"> ✎</a>"
            . 'Voiture d\'immatriculation'
            . "<a  href=\"FrontController.php?action=read&immatriculation={$immatriculationUrl}\"> {$immatriculationHtml} </a>"
            . "<a  href=\"FrontController.php?action=delete&immatriculation={$immatriculationUrl}\"> 🗑️</a>"
            . '</p>' . "\n";

    }
?>
