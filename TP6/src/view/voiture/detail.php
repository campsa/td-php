
    <?php
        $immatriculationHtml = htmlspecialchars($voitures->getImmatriculation());

        echo '<p> Voiture d\'immatriculation ' . $immatriculationHtml .
            ' de marque ' . htmlspecialchars($voitures->getMarque())
            .' de Couleur '.htmlspecialchars($voitures->getCouleur()) . '.</p>';
    ?>
