<form method="get" action="frontController.php">
    <legend> Création d'une Voiture :</legend>
    <p>
        <label for="immat_id">Immatriculation</label> :
        <input type="text" placeholder="256AB34" name="immatriculation" id="immat_id" required/>
    </p>
    <p>
        <input type="hidden" value="created" name="action">
    </p>
    <p>
        <label for="mar">Marque :</label>
        <input type="text" placeholder="Renault" name="marque" id="mar" required/>
    </p>
    <p>
        <label for="coul">Couleur :</label>
        <input type="text" placeholder="rouge" name="couleur" id="coul" required/>
    </p>
    <p>
        <label for="nbs">Nombre de sièges :</label>
        <input type="number" placeholder="5" name="nbSiege" id="nbs" required/>
    </p>
    <p>
        <input id="submit" type="submit" value="Envoyer" />
    </p>
</form>

