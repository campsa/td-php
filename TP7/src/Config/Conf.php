<?php
namespace App\Covoiturage\Config;
class Conf {

    static private array $databases = array(
        // Le nom d'hote est webinfo a l'IUT
        // ou localhost sur votre machine
        //
        // ou webinfo.iutmontp.univ-montp2.fr
        // pour accéder à webinfo depuis l'extérieur
        'hostname' => 'webinfo.iutmontp.univ-montp2.fr',
        // A l'IUT, vous avez une BDD nommee comme votre login
        // Sur votre machine, vous devrez creer une BDD
        'database' => 'campsa',
        // A l'IUT, c'est votre login
        // Sur votre machine, vous avez surement un compte 'root'
        'login' => 'campsa',
        // A l'IUT, c'est votre mdp (INE par defaut)
        // Sur votre machine personelle, vous avez creez ce mdp a l'installation
        'password' => '080427965HA'
    );

    static public function getLogin() : string {
        //en PHP l'indice d'un tableau n'est pas forcement un chiffre.
        return self::$databases['login'];
    }
    //Returning self from a method simply means that your method returns a
    // reference to the instance object on which it was called.
    static public function getHostname() : string {
        return self::$databases['hostname'];
    }

    static public function getDatabase() : string {
        return self:: $databases['database'];
    }

    static public function getPassword() : string {
        return self::$databases['password'];
    }

}
