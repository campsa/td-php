<?php
namespace App\Covoiturage\controller;
use App\Covoiturage\Model\Repository\AbstractRepository;
use App\Covoiturage\Model\Repository\VoitureRepository;
use App\Covoiturage\Model\DataObject\Voiture;

class ControllerVoiture extends GenericController {
    /*
    protected static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../view/$cheminVue"; // Charge la vue

    }
    */


    public static function readAll() {
        $voitures = (new VoitureRepository())->selectAll();//appel au modèle pour gerer la BD
        GenericController::afficheVue('../view/view.php',['pagetitle' => "Liste des voitures", 'cheminVueBody' => "voiture/list.php", 'voitures' => $voitures]);

    }

    public static function create() {

        GenericController::afficheVue('../view/view.php', ['pagetitle' => "Crée une voiture", 'cheminVueBody' => "voiture/create.php"]);
    }

    public static function created() {

        $modelVoiture = new Voiture($_GET['marque'], $_GET['couleur'], $_GET['immatriculation'], $_GET['nbSiege']);
        (new VoitureRepository())->sauvegarder($modelVoiture);
        $voitures = (new VoitureRepository())->selectAll();
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "Voiture créée", 'cheminVueBody' => "voiture/created.php", 'voitures' => $voitures]);

    }

    public static function delete() {

        if(isset($_GET['immatriculation'])) {
            (new VoitureRepository())->delete($_GET['immatriculation']);
            $voitures = (new VoitureRepository())->selectAll();
            GenericController::afficheVue('../view/view.php', ['pagetitle' => "Voiture suppriméée", 'cheminVueBody' => "voiture/deleted.php", 'voitures' => $voitures, 'immatriculation' => $_GET['immatriculation']]);        }
        else {
            echo "il manque l'immatriculation !";
        }

    }

    public static function read() {

        if(isset($_GET['immatriculation'])) {

            $voiture = (new VoitureRepository())->select($_GET['immatriculation']);

            if (is_null($voiture)) {

                GenericController::afficheVue('../view/view', ['pagetitle' => "Erreur", 'cheminVueBody' => "voiture/error.php"]);
                //         }
            }
            else {

                GenericController::afficheVue('../view/view.php',['pagetitle' => "Detail - Voiture", 'cheminVueBody' => "voiture/detail.php", 'voiture' => $voiture]);

            }

        }

        else {

            echo "aucune immatriculation renseignée !";

        }
    }

    public static function update()
    {
        GenericController::afficheVue('../view/view.php', [
            'pagetitle' => "Modifier une voiture",
            'cheminVueBody' => "voiture/update.php",
            'v' => (new VoitureRepository())->select($_GET['immatriculation'])
        ]);


    }

    public static function updated() {

        $modelVoiture = new Voiture($_GET['marque'], $_GET['couleur'], $_GET['immatriculation'], $_GET['nbSieges']);
        (new VoitureRepository)->update($modelVoiture);
        $voitures = (new VoitureRepository())->selectAll();
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "Voiture créée", 'cheminVueBody' => "voiture/updated.php", 'voitures' => $voitures]);

    }


    public static function test(){
        echo "affiche";
    }
}
?>