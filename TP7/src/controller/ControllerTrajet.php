<?php

namespace App\Covoiturage\controller;
use App\Covoiturage\Model\DataObject\Trajet;
use App\Covoiturage\Model\DataObject\Utilisateur;
use App\Covoiturage\Model\Repository\AbstractRepository;
use App\Covoiturage\Model\Repository\TrajetRepository;
use App\Covoiturage\Model\Repository\UtilisateurRepository;

class ControllerTrajet extends GenericController
{
    /*
    protected static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../view/$cheminVue"; // Charge la vue

    }
    */

    public static function readAll() {
        $trajets = (new TrajetRepository())->selectAll();//appel au modèle pour gerer la BD
        GenericController::afficheVue('../view/view.php',['pagetitle' => "Liste des trajet", 'cheminVueBody' => "trajet/list.php", 'trajets' => $trajets]);

    }

    public static function read() {
        if(isset($_GET['id'])) {
            $trajet = (new TrajetRepository())->select($_GET['id']);
            if (is_null($trajet)) {
                GenericController::afficheVue('../view/view', ['pagetitle' => "Erreur", 'cheminVueBody' => "trajet/error.php"]);
            }
            else {
                GenericController::afficheVue('../view/view.php',['pagetitle' => "Detail - trajet", 'cheminVueBody' => "trajet/detail.php", 'trajet' => $trajet]);
            }
        }
        else {
            echo "aucun id renseignée !";
        }
    }
    public static function delete() {
        if(isset($_GET['id'])) {
            (new TrajetRepository())->delete($_GET['id']);
            $trajets = (new TrajetRepository())->selectAll();
            GenericController::afficheVue('../view/view.php', ['pagetitle' => "Trajet supprimé", 'cheminVueBody' => "trajet/deleted.php", 'trajets' => $trajets, 'trajet' => $_GET['id']]);        }
        else {
            echo "il manque l' id !";
        }
    }

    public static function updated()
    {
        $date = ''.$_GET['date'];
        $modelTrajet = new Trajet($_GET['id'], $_GET['depart'], $_GET['arrivee'], $date, $_GET['nbPlaces'], $_GET['prix'], $_GET['conducteurLogin']);
        (new TrajetRepository())->update($modelTrajet);
        $trajets = (new TrajetRepository())->selectAll();
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "trajet créé", 'cheminVueBody' => "trajet/updated.php", 'trajets' => $trajets]);
    }

    public static function update()
    {
        GenericController::afficheVue('../view/view.php', [
            'pagetitle' => "Modifier un Trajet",
            'cheminVueBody' => "trajet/update.php",
            'trajet' => (new TrajetRepository())->select($_GET['id'])
        ]);
    }

    public static function create() {
        $userLogin = (new UtilisateurRepository())->selectAllId();
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "Crée un trajet", 'cheminVueBody' => "trajet/create.php", 'userLogin' => $userLogin]);
    }

    public static function created() {
        $date = ''.$_GET['date'];
        $modelTrajet = new Trajet($_GET['id'], $_GET['depart'], $_GET['arrive'], $date, $_GET['nbPlaces'], $_GET['prix'], $_GET['conducteurLogin']);
        (new TrajetRepository())->sauvegarder($modelTrajet);
        $trajets = (new TrajetRepository())->selectAll();
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "trajet créé", 'cheminVueBody' => "trajet/created.php", 'trajets' => $trajets]);

    }

}
?>