<?php

namespace App\Covoiturage\controller;
use App\Covoiturage\Model\DataObject\Voiture;
use App\Covoiturage\Model\HTTP\Session;
use App\Covoiturage\Model\Repository\AbstractRepository;
use App\Covoiturage\Model\Repository\UtilisateurRepository;
use App\Covoiturage\Model\DataObject\Utilisateur;
use App\Covoiturage\Model\Repository\VoitureRepository;

use App\Covoiturage\Model\HTTP\Cookie;

class ControllerUtilisateur extends GenericController
{
    /*
    protected static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../view/$cheminVue"; // Charge la vue

    }
    */


    public static function readAll() {
        $utilisateurs = (new UtilisateurRepository())->selectAll();//appel au modèle pour gerer la BD
        GenericController::afficheVue('../view/view.php',['pagetitle' => "Liste des voitures", 'cheminVueBody' => "utilisateur/list.php", 'utilisateurs' => $utilisateurs]);

    }

    public static function read() {

        if(isset($_GET['login'])) {
            $utilisateur = (new UtilisateurRepository())->select($_GET['login']);
            if (is_null($utilisateur)) {
                GenericController::afficheVue('../view/view', ['pagetitle' => "Erreur", 'cheminVueBody' => "utilisateur/error.php"]);
            }
            else {
                GenericController::afficheVue('../view/view.php',['pagetitle' => "Detail - utilisateur", 'cheminVueBody' => "utilisateur/detail.php", 'utilisateur' => $utilisateur]);
            }
        }
        else {
            echo "aucun login renseignée !";
        }
    }

    public static function delete() {

        if(isset($_GET['login'])) {
            (new UtilisateurRepository())->delete($_GET['login']);
            $utilisateurs = (new UtilisateurRepository())->selectAll();
            GenericController::afficheVue('../view/view.php', ['pagetitle' => "Utilisateur supprimé", 'cheminVueBody' => "utilisateur/deleted.php", 'utilisateurs' => $utilisateurs, 'user' => $_GET['login']]);        }
        else {
            echo "il manque le login !";
        }

    }

    public static function updated()
    {

        $modelUtilisateur = new Utilisateur($_GET['login'], $_GET['prenom'], $_GET['nom']);
        (new UtilisateurRepository)->update($modelUtilisateur);
        $utilisateurs = (new UtilisateurRepository())->selectAll();
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "utilisateur créée", 'cheminVueBody' => "utilisateur/updated.php", 'utilisateurs' => $utilisateurs]);
    }

    public static function update()
    {
        GenericController::afficheVue('../view/view.php', [
            'pagetitle' => "Modifier un Utilisateur",
            'cheminVueBody' => "utilisateur/update.php",
            'utilisateur' => (new UtilisateurRepository())->select($_GET['login'])
        ]);

    }

    public static function create() {
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "Crée un utilisateur", 'cheminVueBody' => "utilisateur/create.php"]);
    }

    public static function created() {

        $modelUtilisateur = new Utilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        (new UtilisateurRepository())->sauvegarder($modelUtilisateur);
        $utilisateurs = (new UtilisateurRepository())->selectAll();
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "Utilisateur créée", 'cheminVueBody' => "utilisateur/created.php", 'utilisateurs' => $utilisateurs]);
    }
    /*
    public static function deposerCookie(){
        Cookie::enregistrer("TestCookie", "OK", time() + 3600);
    }

    public static function lireCookie(){
        echo Cookie::lire("TestCookie");

    }

    public static function test(){
        echo "affiche";
    }
    */

    public static function sessionCre(){
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Cathy Penneflamme");


    }
    public static function sessLire(){
        $session = Session::getInstance();
        $op=$session->lire("utilisateur");
        echo "<p>". " $op" ."</p>";
    }

}