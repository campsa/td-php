<?php

namespace App\Covoiturage\controller;

use App\Covoiturage\Lib\PreferenceControleur;

class GenericController{

    protected static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../view/$cheminVue"; // Charge la vue

    }

    public static function error($erreur) : void{
        GenericController::afficheVue('../view/view.php', ['pagetitle' => "Erreur", 'cheminVueBody' => "voiture/error.php", 'errorMessage' => $erreur]);
    }

    public static function formulairePreference() : void{
        self::afficheVue('../view/view.php',['pagetitle' => "Préférence", 'cheminVueBody' => "formulairePreference.php"]);
    }

    public static function enregistrerPreference() : void{
        $pref = $_POST['controleur_defaut'];
        PreferenceControleur::enregistrer($pref);
        self::afficheVue('../view/view.php',['pagetitle' => "enregistrementPref", 'cheminVueBody' => "enregistrerPreference.php"]);
    }

}