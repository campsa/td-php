<?php

namespace App\Covoiturage\Model\HTTP;

class Cookie{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void{

        $newValue = serialize($valeur);
        if($dureeExpiration != null)
            setcookie($cle, $newValue, time() + $dureeExpiration);
        else{
            setcookie($cle, $newValue);
        }
    }

    public static function lire(string $cle): mixed{
         return unserialize($_COOKIE[$cle]);
    }

    public static function contient($cle) : bool{
        if(isset($_COOKIE[$cle])){
            return true;
        }
        return false;
    }
    public static function supprimer($cle) : void{
        if(self::contient($cle)){
            unset($_COOKIE[$cle]);
            setcookie ($cle, "", 1);
        }
    }












}

?>

