<?php

namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\DataObject\Voiture;
use App\Covoiturage\Model\Repository\DatabaseConnection as Model;

class VoitureRepository extends AbstractRepository
{

    public static function getVoitures()
    {

        $pdo = Model::getPdo();
        $query = "SELECT * FROM voiture;";
        $pdoStatement = $pdo->query($query);

        $tab = [];

        foreach ($pdoStatement as $voitureFormatTableau) {

            $tab[] = self::construire($voitureFormatTableau);

        }

        return $tab;

    }

    public static function getVoitureParImmat(string $immatriculation): ?Voiture
    {

        $sql = "SELECT * from voiture WHERE immatriculation=:immatriculationTag";
        // Préparation de la requête
        $pdoStatement = Model::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();

        if (!$voiture) {

            $res = null;

        } else {

            $res = static::construire($voiture);

        }

        return $res;

    }



    protected function construire(array $voitureFormatTableau): Voiture
    {

        return new Voiture ($voitureFormatTableau['marque'],
            $voitureFormatTableau['couleur'],
            $voitureFormatTableau['immatriculation'],
            $voitureFormatTableau['nbSieges']);
    }



    public static function supprimerParImmatriculation(string $immatriculation): bool {

        try {

            $pdoStatement = Model::getPdo()->prepare("DELETE FROM voiture WHERE immatriculation = :imm;");
            $pdoStatement->execute(array(
                'imm' => $immatriculation,
            ));

        } catch (PDOException $exeption) {

            return false;
        }

        return true;

    }

    protected function getNomTable(): string {

        return 'voiture';
    }

    protected function getNomClePrimaire(): string {

        return 'immatriculation';
    }

    protected function getNomsColonnes(): array {

        return [ 0 => 'marque',
            1 => 'couleur',
            2 => 'immatriculation',
            3 => 'nbSieges'];
    }



}