<?php

namespace App\Covoiturage\Model\Repository;
use App\Covoiturage\Model\DataObject\Trajet;
use App\Covoiturage\Model\Repository\DatabaseConnection as Model;

class TrajetRepository extends AbstractRepository
{
    protected function getNomTable(): string {

        return 'trajet';
    }

    protected function construire(array $userFormatTableau) : Trajet
    {
        return new Trajet($userFormatTableau['id'],
            $userFormatTableau['depart'],
            $userFormatTableau['arrive'],
            $userFormatTableau['date'],
            $userFormatTableau['nbPlaces'],
            $userFormatTableau['prix'],
            $userFormatTableau['conducteurLogin']);
    }

    protected function getNomClePrimaire(): string {
        return 'id';
    }

    protected function getNomsColonnes(): array {
        return [ 0 => 'id',
            1 => 'depart',
            2 => 'arrive',
            3 => 'date',
            4 => 'nbPlaces',
            5 => 'prix',
            6 => 'conducteurLogin',
        ];
    }
}