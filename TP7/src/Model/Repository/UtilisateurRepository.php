<?php

namespace App\Covoiturage\Model\Repository;
use App\Covoiturage\Model\DataObject\Utilisateur;
use App\Covoiturage\Model\Repository\DatabaseConnection as Model;

class UtilisateurRepository extends AbstractRepository {


    protected function getNomTable(): string {

        return 'utilisateur';
    }

    protected function construire(array $userFormatTableau) : Utilisateur
    {
        return new Utilisateur($userFormatTableau['login'],
            $userFormatTableau['nom'],
            $userFormatTableau['prenom']);
    }

    protected function getNomClePrimaire(): string {

        return 'login';
    }


    protected function getNomsColonnes(): array {

        return [ 0 => 'login',
            1 => 'nom',
            2 => 'prenom',
        ];
    }

    public function selectAllId()
    {

        $pdo = Model::getPdo();
        $query = "SELECT login FROM utilisateur;";
        $pdoStatement = $pdo->query($query);

        $tab = [];
        foreach ($pdoStatement as $voitureFormatTableau) {

            $tab[] = $voitureFormatTableau;

        }

        return $tab;

    }
}