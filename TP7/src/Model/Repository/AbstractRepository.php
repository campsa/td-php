<?php

namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\DataObject\Voiture;
use App\Covoiturage\Model\Repository\DatabaseConnection as Model;

abstract class AbstractRepository
{
    public function selectAll()
    {

        $pdo = Model::getPdo();
        $query = "SELECT * FROM " . static::getNomTable() . ";";
        $pdoStatement = $pdo->query($query);

        $tab = [];

        foreach ($pdoStatement as $voitureFormatTableau) {

            $tab[] = static::construire($voitureFormatTableau);

        }

        return $tab;

    }

    public function select(string $immatriculation)
    {
        $sql = " SELECT * FROM " .  static::getNomTable() . " WHERE " .  static::getNomClePrimaire() . "=:immatriculation";
        // Préparation de la requête
        $pdoStatement = Model::getPdo()->prepare($sql);

        $values = array(
            "immatriculation" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();

        if (!$voiture) {
            $res = null;
        } else {
            $res = static::construire($voiture);
        }
        return $res;
    }

    public function delete($valeurClePrimaire) {

        $sql = " DELETE FROM " .  static::getNomTable() . " WHERE " .  static::getNomClePrimaire() . "=:immatriculation";
        // Préparation de la requête
        $pdoStatement = Model::getPdo()->prepare($sql);
        $values = array(
            "immatriculation" => $valeurClePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();
        if (!$voiture) {
            $res = null;
        } else {
            $res = static::construire($voiture);
        }
        return $res;
    }


    public function update($voiture){

        try {
            $pdo = Model::getPdo();
            $sql = "UPDATE " . static::getNomTable() . " SET ";
            //marque = :mar, couleur = :coul, nbsieges = :nbs WHERE " .  $this->getNomClePrimaire() . "= :imm; ";
            foreach (static::getNomsColonnes() as $i) {
                if (!strcmp($i, static::getNomClePrimaire()) == 0) {
                    $sql .= $i . " = :" . $i . " , ";
                }
            }
            $sql = rtrim($sql,' , ');
            $sql .= " WHERE " . static::getNomClePrimaire() . " =:" . static::getNomClePrimaire() . " ; ";
            $pdoStatement = $pdo->prepare($sql);
            $tab = $voiture->formatTableau();
            $pdoStatement->execute($tab);
            return true;
        } catch (PDOException $exception) {
            echo $exception->getMessage();
            return false;
        }
    }
// INSERT INTO nomTable (champ1, champ2) VALUES (:champ1,:champ2);
    public function sauvegarder($voiture): bool {
        try {
            $pdo = Model::getPdo();
            $sql = "INSERT INTO " . static::getNomTable() . " (";
            foreach (static::getNomsColonnes() as $i) {
                $sql .= $i . " , ";
            }
            $sql = rtrim($sql,' , ');
            $sql .= ") VALUES (";
            foreach (static::getNomsColonnes() as $i) {
                $sql .= " :" . $i . " , ";
            }
            $sql = rtrim($sql,' , ');
            $sql.= ")";
            $pdoStatement = $pdo->prepare($sql);
            $tab = $voiture->formatTableau();
            $pdoStatement->execute($tab);
            return true;
        } catch (PDOException $exception) {
            echo $exception->getMessage();
            return false;
        }
    }

    protected abstract function getNomTable(): string;

    protected abstract function construire(array $objetFormatTableau);

    protected abstract function getNomClePrimaire(): string;

    protected abstract function getNomsColonnes(): array;

}