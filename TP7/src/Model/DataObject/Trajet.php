<?php

namespace App\Covoiturage\Model\DataObject;


class Trajet extends AbstractDataObject
{
    public int $id;
    public string $depart;
    public string $arrive;
    public string $date;
    public int $nbPlaces;
    public int $prix;
    public string $conducteurLogin;

    public function __construct(int $id, string $depart, string $arrive, string $date, int $nbPlaces, int $prix, string $conducteurLogin)
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrive = $arrive;
        $this->date = $date;
        $this->nbPlaces = $nbPlaces;
        $this->prix = $prix;
        $this->conducteurLogin = $conducteurLogin;
    }

    public static function getAllTrajet()
    {
        $pdo = Model::getPdo();
        $query = "SELECT * FROM trajet;";
        $pdoStatement = $pdo->query($query);
        $tab = [];
        foreach ($pdoStatement as $userFormatTableau) {
            $tab[] = self::builder($userFormatTableau);
        }
        return $tab;
    }

    public static function getTrajets($id)
    {
        $pdo = Model::getPdo();
        $sql = "SELECT * 
                FROM trajet t  
                JOIN passager p ON t.id = p.trajetId
                JOIN utilisateur u ON p.utilisateurLogin=u.login
                WHERE t.id = :id";
        $reponse = $pdo->prepare($sql);
        $reponse->execute(array('id' => $id));
        $tab = [];
        foreach ($reponse as $trajetFormatTableau) {
            $tab[] = Trajet::construire($trajetFormatTableau);
        }
        return $tab;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDepart(): string
    {
        return $this->depart;
    }

    /**
     * @param string $depart
     */
    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    /**
     * @return string
     */
    public function getArrive(): string
    {
        return $this->arrive;
    }

    /**
     * @param string $arrive
     */
    public function setArrive(string $arrive): void
    {
        $this->arrive = $arrive;
    }


    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getNbPlaces(): string
    {
        return $this->nbPlaces;
    }

    /**
     * @param string $nbPlaces
     */
    public function setNbPlaces(string $nbPlaces): void
    {
        $this->nbPlaces = $nbPlaces;
    }

    /**
     * @return string
     */
    public function getPrix(): string
    {
        return $this->prix;
    }

    /**
     * @param string $prix
     */
    public function setPrix(string $prix): void
    {
        $this->prix = $prix;
    }

    /**
     * @return string
     */
    public function getConducteurLogin(): string
    {
        return $this->conducteurLogin;
    }

    /**
     * @param string $conducteurLogin
     */
    public function setConducteurLogin(string $conducteurLogin): void
    {
        $this->conducteurLogin = $conducteurLogin;
    }

    public function afficher(): void
    {
        echo "<div> id = $this->id </div> <br>\n";
        echo "<div> Depart = $this->depart </div> <br>\n";
        echo "<div> Arrivee = $this->arrivee </div> <br>\n";
        echo "<div> Date = $this->date </div> <br>\n";
        echo "<div> $this->nbPlaces</div> places disponibles <br>\n";
        echo "<div> $this->prix</div> € <br>\n";
        echo "<div> Login du conducteur = $this->conducteurLogin </div> <br>\n";
    }

    public function formatTableau(): array
    {

        return ['id' => $this->getId(),
            'depart' => $this->getDepart(),
            'arrive' => $this->getArrive(),
            'date' => $this->getDate(),
            'nbPlaces' => $this->getNbPlaces(),
            'prix' => $this->getPrix(),
            'conducteurLogin' => $this->getConducteurLogin(),
        ];
    }

}