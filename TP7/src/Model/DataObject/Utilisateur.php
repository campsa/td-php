<?php
namespace App\Covoiturage\Model\DataObject;

class Utilisateur extends AbstractDataObject {

    public String $login;
    public String $prenom;
    public String $nom;

    public function __construct(string $login, string $prenom, string $nom)
    {
        $this->login = $login;
        $this->prenom = $prenom;
        $this->nom = $nom;
    }


    public static function getAllUtilisateurs() {

        $pdo = Model::getPdo();
        $query = "SELECT * FROM utilisateur;";
        $pdoStatement = $pdo->query($query);

        $tab = [];

        foreach ($pdoStatement as $userFormatTableau) {

            $tab[] = self::builder($userFormatTableau);

        }

        return $tab;

    }

    public static function getTrajets($login) {

        $pdo = Model::getPdo();

        $sql = "SELECT * 
                FROM trajet t  
                JOIN passager p ON t.id = p.trajetId
                JOIN utilisateur u ON p.utilisateurLogin=u.login
                WHERE u.login = :login";

        $reponse = $pdo->prepare($sql);
        $reponse->execute(array('login'=>$login));
        //$reponse->fetch();


        $tab = [];

        foreach ($reponse as $trajetFormatTableau) {

            $tab[] = Trajet::construire($trajetFormatTableau);

        }

        return $tab;
    }

    /**
     * @return String
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param String $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return String
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param String $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return String
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param String $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function afficher() : void {

        echo "<div> Login = $this->login </div> <br>\n";
        echo "<div> Nom = $this->nom </div> <br>\n";
        echo "<div> Prenom = $this->prenom </div> <br>\n";
    }

    public function formatTableau(): array {

        return ['login' => $this->getLogin(),
            'nom' => $this->getNom(),
            'prenom' => $this->getPrenom(),
        ];
    }
}
?>