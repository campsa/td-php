<?php
namespace App\Covoiturage\Model\DataObject;

class Voiture extends AbstractDataObject
{

    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque(): string
    {
        return $this->marque;
    }

    // un setter
    public function setMarque($marque): void
    {
        $this->marque = $marque;
    }

    // un constructeur
    public function __construct(
        string $marque,
        string $couleur,
        string $immatriculation,
        int    $nbSieges
    )
    {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = $immatriculation;
        $this->nbSieges = $nbSieges;
    }

    public function formatTableau(): array {

        return ['marque' => $this->getMarque(),
            'couleur' => $this->getCouleur(),
            'immatriculation' => $this->getImmatriculation(),
            'nbSieges' => $this->getNbSieges()
        ];
    }


    /**
     * @return mixed
     */
    public function getCouleur(): string
    {
        return $this->couleur;
    }

    /**
     * @param mixed $couleur
     */
    public function setCouleur($couleur): void
    {
        $this->couleur = $couleur;
    }

    /**
     * @return mixed
     */
    public function getImmatriculation(): string
    {
        return $this->immatriculation;
    }

    /**
     * @param mixed $immatriculation
     */
    public function setImmatriculation($immatriculation): void
    {
        $this->immatriculation = $immatriculation;
    }

    public function setImmatriculationBis($immatriculation): void
    {
        $this->immatriculation = substr($immatriculation, 0, 7);
    }

    /**
     * @return mixed
     */
    public function getNbSieges(): int
    {
        return $this->nbSieges;
    }

    /**
     * @param mixed $nbSieges
     */
    public function setNbSieges($nbSieges): void
    {
        $this->nbSieges = $nbSieges;
    }

    // une methode d'affichage.
    /* public function afficher(): void {
         echo "<> $this->marque " . " $this->immatriculation " . " $this->couleur " . " $this->nbSieges </p>";
     }*/

}

?>