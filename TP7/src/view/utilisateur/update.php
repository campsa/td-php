<form method="get" action="frontController.php">

    <legend> Modification d'un Utilisateur :</legend>
    <p>
        <label for="user">Utilisateur</label> :
        <input type="text" placeholder="User" name="login" id="user" value="<?php echo(htmlspecialchars($utilisateur->getLogin())) ?>" readonly />
    </p>
    <p>
        <label for="nom">Nom</label> :
        <input type="text" placeholder="nom" value="<?php echo(htmlspecialchars($utilisateur->getNom())) ?>" name="nom" id="n" required/>
    </p>
    <p>
        <label for="prenom">Prenom</label> :
        <input type="text" placeholder="prenom" value="<?php echo(htmlspecialchars($utilisateur->getPrenom())) ?>" name="prenom" id="m" required/>
    </p>
    <p>
        <input id="submit" type="submit" value="Envoyer" />
        <input type="hidden" value="updated" name="action">
        <input type="hidden" value="utilisateur" name="controller">
    </p>
</form>