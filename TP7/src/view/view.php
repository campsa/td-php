<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $pagetitle; ?></title>
    <link rel="stylesheet" href="../../web/css/styles.css">
</head>
<body
<header>
    <nav>
        <ul class="nav">
            <li><a href="frontController.php?action=readAll&controller=voiture">Voiture</a></li>
            <li><a href="frontController.php?action=readAll&controller=utilisateur">Utilisateur</a></li>
            <li><a href="frontController.php?action=readAll&controller=trajet">Trajet</a></li>
            <li><a href="frontController.php?action=formulairePreference "><img src="../web/css/heart.png"></a></li>
        </ul>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</main>
<footer>
    <p>Site de covoiturage de Zinedine</p>
</footer>
</body>
</html>