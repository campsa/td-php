
<?php
use App\Covoiturage\Lib\PreferenceControleur;
$existe = PreferenceControleur::existe();
?>
<form method="post" action="frontController.php?action=enregistrerPreference">
    <legend> Preference :</legend>
    <p>
        <input type="radio" id="voitureId" name="controleur_defaut" value="voiture" <?php if($existe and PreferenceControleur::lire()=="voiture"){ echo "checked"; } ?> >
        <label for="voitureId">Voiture</label>
    </p>
    <p>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"<?php if($existe and PreferenceControleur::lire()=="utilisateur"){ echo "checked"; } ?> >
        <label for="utilisateurId">Utilisateur</label>
    </p>
    <p>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php if($existe and PreferenceControleur::lire()=="trajet"){ echo "checked"; } ?> >
        <label for="trajetId">Trajet</label>
    </p>

    <input type="hidden" value="created" name="action">
    <input type="hidden" value="trajet" name="controller">
    <input id="submit" type="submit" value="Envoyer" />
    </p>
</form>

