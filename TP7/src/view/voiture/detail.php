
<?php
$immatriculationHTML = htmlspecialchars($voiture->getImmatriculation());
$marqueHTML = htmlspecialchars($voiture->getMarque());
$couleurHTML = htmlspecialchars($voiture->getCouleur());
$nbsiegeHTML = htmlspecialchars($voiture->getNbSieges());
echo '<p> Voiture d\'immatriculation ' . $immatriculationHTML . '.</p>' .
    '<p> Voiture de marque ' . $marqueHTML . '.</p>' .
    '<p> Voiture de couleur ' . $couleurHTML . '.</p>' .
    '<p> Voiture ayant ' . $nbsiegeHTML . ' Sieges' . '.</p>'
?>