<form method="get" action="frontController.php">

    <legend> Modification d'un Trajet :</legend>
    <p>
        <label for="id">Trajet</label> :
        <input type="text" placeholder="identificateur" name="id" id="id" value="<?php echo(htmlspecialchars($trajet->getId())) ?>" readonly />
    </p>
    <p>
        <label for="depart">Départ</label> :
        <input type="text" placeholder="Montarnaud" value="<?php echo(htmlspecialchars($trajet->getDepart())) ?>" name="depart" id="n" required/>
    </p>
    <p>
        <label for="ar">Arrivée</label> :
        <input type="text" placeholder="Sète" value="<?php echo(htmlspecialchars($trajet->getArrive())) ?>" name="arrivee" id="ar" required/>
    </p>
    <p>
        <label for="date">Date</label> :
        <input type="date" placeholder="15/20/2023" value="<?php echo(htmlspecialchars($trajet->getDate())) ?>" name="date" id="date" required/>
    </p>
    <p>
        <label for="nbp">Nombre de Places</label> :
        <input type="number" placeholder="3" value="<?php echo(htmlspecialchars($trajet->getnbPlaces())) ?>" name="nbPlaces" id="nbp" required/>
    </p>
    <p>
        <label for="prix">Prix</label> :
        <input type="text" placeholder="15" value="<?php echo(htmlspecialchars($trajet->getPrix())) ?>" name="prix" id="prix" required/>
    </p>
    <p>
        <label for="lcd">Login du Conducteur</label> :
        <input type="text" placeholder="pierreg" value="<?php echo(htmlspecialchars($trajet->getConducteurLogin())) ?>" name="conducteurLogin" id="lcd" required/>
    </p>
    <p>
        <input id="submit" type="submit" value="Envoyer" />
        <input type="hidden" value="updated" name="action">
        <input type="hidden" value="trajet" name="controller">
    </p>
</form>