<?php
$idHTML = htmlspecialchars($trajet->getId());
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveHTML = htmlspecialchars($trajet->getArrive());
$dateHTML = htmlspecialchars($trajet->getDate());
$nbpHTML = htmlspecialchars($trajet->getnbPlaces());
$prixHTML = htmlspecialchars($trajet->getPrix());
$clHTML = htmlspecialchars($trajet->getConducteurLogin());

echo ' <p> Trajet d\' id ' . $idHTML . ' </p>' .
    ' <p> Départ à ' . $departHTML . ' </p>' .
    '<p> Arrivée à ' . $arriveHTML . ' </p>' .
    '<p> Le ' . $dateHTML . ' </p>' .
    '<p>' . $nbpHTML . ' places disponibles </p>' .
    '<p> Prix = ' . $prixHTML . '€ </p>' .
    '<p> Login du Conducteur ' . $clHTML . ' </p>';
?>