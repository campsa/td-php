<form method="get" action="frontController.php">
    <legend> Création d'un Trajet :</legend>
    <p>
        <label for="id">id :</label>
        <input type="text" placeholder="pierreg" name="id" id="id" required/>
    </p>
    <p>
        <label for="ville1">Départ :</label>
        <input type="text" placeholder="Montarnaud" name="depart" id="ville1" required/>
    </p>
    <p>
        <label for="ville2">Arrivée :</label>
        <input type="text" placeholder="Sète" name="arrive" id="ville2" required/>
    </p>
    <p>
        <label for="date">Date :</label>
        <input type="date"  name="date" id="date" required/>
    </p>
    <p>
        <label for="nbp">Nombre de places :</label>
        <input type="number" placeholder="5" name="nbPlaces" id="nbp" required/>
    </p>
    <p>
        <label for="prix">Prix :</label>
        <input type="text" placeholder="15" name="prix" id="prix" required/>
    </p>
    <p>
        <label for="ldc">Login du Conducteur :</label>
        <select name="conducteurLogin" id="conducteurLogin" required>

            <?php
            foreach ($userLogin as $value){
                ?><option value="<?php echo($value['login']); ?>"><?php echo($value['login']); ?></option><?php
            }
            ?>
        </select>
    </p>
    <input type="hidden" value="created" name="action">
    <input type="hidden" value="trajet" name="controller">
    <input id="submit" type="submit" value="Envoyer" />
    </p>
</form>