<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
use App\Covoiturage\controller\ControllerVoiture;
use App\Covoiturage\Controller\ControllerUtilisateur;
use App\Covoiturage\controller\GenericController;
use App\Covoiturage\Lib\PreferenceControleur;


// instantiate the loader
$loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
// register the base directories for the namespace prefix
$loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');
// register the autoloader
$loader->register();

if (isset($_GET['action'])) {

    $action = $_GET['action']; // $_GET['action'] récupère l'action saisie dans l'URL
} else {
    $action = 'readAll'; // $_GET['action'] récupère l'action saisie dans l'URL
}

if (isset($_GET['controller'])) {
    $controller = $_GET['controller'];

}
else if (PreferenceControleur::existe()){
    $controller = PreferenceControleur::lire();
}
else {
    $controller = 'voiture';
}
$controllerClassName = 'App\Covoiturage\controller\Controller' . ucfirst($controller);

if (class_exists($controllerClassName)) {
    if (in_array($action, get_class_methods($controllerClassName))) {
        $controllerClassName::$action();
    } else {
        GenericController::error("action inexistante");
    }
}
else {
    GenericController::error("controlleur inexistante");
}


?>