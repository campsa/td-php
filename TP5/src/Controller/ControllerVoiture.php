<?php

namespace App\Covoiturage\Controller;
use App\Covoiturage\Model\ModelVoiture;

class ControllerVoiture {

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../src/view/$cheminVue"; // Charge la vue
    }


    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $voitures = ModelVoiture::getVoitures(); //appel au modèle pour gerer la BD

        self::afficheVue('/view.php',['voitures' => $voitures,"pagetitle" => "Liste des voitures", "cheminVueBody" => "voiture/list.php"]);  //"redirige" vers la vue
    }

    public static function read() : void{
        $immatriculation=$_GET['immat'];
        $voitures = ModelVoiture::getVoitureParImmat($immatriculation);
        if(!$voitures){
            self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "voiture/error.php"]);
        }
        else {
            self::afficheVue('/view.php',['voitures' => $voitures,"pagetitle"=>"Voiture","cheminVueBody"=>"voiture/detail.php"]);
        }
    }

    public static function create() : void{
        self::afficheVue('/view.php',["pagetitle"=>"Creer une voiture","cheminVueBody"=>"voiture/create.php"]);
    }

    public static function created() : void{
        /*
        $voiture = new ModelVoiture($_GET['marque'],$_GET['couleur'],$_GET['immatriculation'],$_GET['nbSieges']);
        $voiture->sauvegarder();
        self::readAll();
        */
        if (isset($_GET['marque']) && isset($_GET['couleur']) && isset($_GET['immatriculation']) && isset($_GET['nbSieges'])) {
            $marque = $_GET['marque'];
            $couleur = $_GET['couleur'];
            $imm = $_GET['immatriculation'];
            $sieges = $_GET['nbSieges'];

            $v = new ModelVoiture($marque, $couleur, $imm, $sieges);
            $bool = $v->sauvegarder();
            if($bool) {
                self::afficheVue('/view.php',["pagetitle" => "Création voiture","cheminVueBody" =>"voiture/created.php"]);

            }
            else{
                self::afficheVue('/view.php',["pagetitle" => "erreur","cheminVueBody" => "voiture/error.php"]);
            }
        } else {
            echo "Wrong data !!";
        }

    }

    public static function delete() {
        if (isset($_GET['immat'])) {
            ModelVoiture::delete($_GET['immat']);
            self::readAll();
        } else {
            echo "<p> Veuillez renseigner l'immatriculation !! </p>";
        }
    }





}
?>

