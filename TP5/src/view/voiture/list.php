
<?php
    foreach ($voitures as $voiture) {
        //echo '<p> Voiture d\'immatriculation ' . $voiture->getImmatriculation() . '.</p>';
        $immatriculationHtml = htmlspecialchars($voiture->getImmatriculation());
        $immatriculationUrl = rawurlencode($voiture->getImmatriculation());

        echo '<p> Voiture d\'immatriculation '
            . "<a  href=\"FrontController.php?action=read&immat={$immatriculationUrl}\"> {$immatriculationHtml} </a>"
            . "<a  href=\"FrontController.php?action=delete&immat={$immatriculationUrl}\"> 🗑️</a>"
            . '</p>';

    }
?>
